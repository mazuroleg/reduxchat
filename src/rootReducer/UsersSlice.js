import { createSlice,createAsyncThunk } from "@reduxjs/toolkit";
export const fetchUsers=createAsyncThunk(
    'chat/fetchUsers',
    async function(_,{rejectWithValue}){
       try{ 
            const response =await fetch('https://edikdolynskyi.github.io/react_sources/messages.json');
                if(!response.ok){
                throw new Error('Server Error');
           }
            const data =await response.json();
            return data;
        }catch(error){
            return rejectWithValue(error.message);
        }
    }

)
export const addNewUser=createAsyncThunk(
    'users/addNewUser',
    async function(_,{rejectWithValue,dispatch}){
       try{ 
           const user={
                email: "mazuroleg75@gmail.com",
                name: "Oleg",
                password: "111111",
                avatar: null
           }
            const response =await fetch('https://bsa-chat.azurewebsites.net/api/Users',{
               method: 'POST',
               headers: 'Content-Type: application/json',
                //   'accept: text/plain',
                  
               
                body:  JSON.stringify(user)
            });
               if(!response.ok){
                throw new Error('cant add user');
           }
            const data =await response.json();
            console.log(data);
        }catch(error){
            return rejectWithValue(error.message);
        }
    })
const usersSlice=createSlice({
    
    name: 'users',
    initialState:{
       users:{}
    },
    reducers:{
        addUsers(state,action){},
        getUsers(state,action){
            // console.log(state);
            // console.log(action);
            // state.chat.messages.push({
            //     //id: new Date().toISOString(),
            //     text:action.payload.text,
            //     createdAt: Date.now(),
            //     id:"myId",
            //     userId: new Date().toISOString(),
            //     user:"Oleh"
            },
        
        removeUsers(state,action){
            // state.chat.messages=state.chat.messages.filter(mes=>mes.userId!==action.payload.userId)
        },
        
      },
    
    extraReducers:{
       [fetchUsers.pending]:(state)=>{
        //   state.status='loading';
        //      state.error=null;
       }, 
       [fetchUsers.fulfilled]:(state,action)=>{
            // state.status='resolved';
            // state.chat.messages=action.payload
       },
       [fetchUsers.rejected]:(state,action)=>{},
    }
})
export const{addUsers,removeUsers}=usersSlice.actions;
export default usersSlice.reducer