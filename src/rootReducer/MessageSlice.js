import { createSlice,createAsyncThunk } from "@reduxjs/toolkit";
export const fetchMessages=createAsyncThunk(
    'chat/fetchMessages',
    async function(_,{rejectWithValue}){
       try{ 
            const response =await fetch('https://edikdolynskyi.github.io/react_sources/messages.json');
                if(!response.ok){
                throw new Error('Server Error');
           }
            const data =await response.json();
            return data;
        }catch(error){
            return rejectWithValue(error.message);
        }
    }

)
const messageSlice=createSlice({
    
    name: 'chat',
    initialState:{
         chat:{
            messages: [
                 {}
            //    {id: "80f08600-1b8f-11e8-9629-c7eca82aa7bd",
            //    userId: "9e243930-83c9-11e9-8e0c-8f1a686f4ce4",
            //    avatar: "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
            //    user: "Ruth",
            //    text: "I don’t *** understand. It's the Panama accounts",
            //    createdAt: "2020-07-16T19:48:12.936Z",
            //    editedAt: ""}
            ],
            admin:true,
            editModal: false,
            preloader: true,
             status:null,
             error:null,
        }
    },
    reducers:{
        setMessages(state,action){},
        addMessage(state,action){
            console.log(state);
            console.log(action);
            state.chat.messages.push({
                //id: new Date().toISOString(),
                text:action.payload.text,
                createdAt: new Date().toISOString(),
                id:"myId",
                userId: new Date().toISOString(),
                user:"Oleh"
            })
        },
        removeMessage(state,action){
            state.chat.messages=state.chat.messages.filter(mes=>mes.userId!==action.payload.userId)
        },
        likeMessage(state,action){},
        editMessage(state,action){},
    },
    extraReducers:{
       [fetchMessages.pending]:(state)=>{
          state.status='loading';
             state.error=null;
       }, 
       [fetchMessages.fulfilled]:(state,action)=>{
            state.status='resolved';
            state.chat.messages=action.payload
       },
       [fetchMessages.rejected]:(state,action)=>{},
    }
})
export const{addMessage,removeMessage}=messageSlice.actions;
export default messageSlice.reducer