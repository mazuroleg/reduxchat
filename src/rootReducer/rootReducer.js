import { configureStore } from "@reduxjs/toolkit";
import messageReducer from "./MessageSlice"
import usersReducer from "./UsersSlice"
import authReducer from "./AuthSlice"
export default configureStore( {
    reducer:{
        messages: messageReducer,
        users: usersReducer,
        auth: authReducer 
    }
})