
import React, { useEffect, useState } from "react";
import Header from "./chatComponents/Header/Header"
import Message from "./chatComponents/Message/Message"
import MessageInput from "./chatComponents/MessageInput/MessageInput"
import MessageList from "./chatComponents/MessageList/MessageList"
import OwnMessage from "./chatComponents/OwnMessage/OwnMessage"
import  "./Chat.css"
import { useDispatch, useSelector } from "react-redux";
import { fetchMessages } from "../rootReducer/MessageSlice";
const Chat=()=>{
    //   const[messages, setMessages]=useState([])
   const dispatch=useDispatch()
   const messages =useSelector(state=>state.messages.chat.messages);
useEffect (() => {
    
    dispatch(fetchMessages());
},[dispatch]);

    return( 
        <div className="chat">
            <div className="header">
                <Header messages={messages}/>
            </div>
            <div>
                
            </div>
            <div ><MessageList messages={messages}  />
                
                  <div >
                
                <MessageInput />
                 </div>   
            </div>
           
            
             
     
        </div>

    )
}
export default Chat