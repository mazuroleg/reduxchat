import "./Header.css"
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
const Header=()=>{
    const messages =useSelector(state=>state.messages.chat.messages);
    const messageCount=messages.length+1;
    const set=new Set(messages)
    const userCount=set.size
    const lastMessageDate=new Date(Date.parse(messages[messages.length-1].createdAt))
    const lastFormDate= lastMessageDate.getDate()+"."
            +lastMessageDate.getMonth()+"."
            +lastMessageDate.getFullYear()+" "
            +lastMessageDate.getHours()+":"
            +lastMessageDate.getMinutes()+":"
            +lastMessageDate.getSeconds()
     //let date= new Date (Date.parse(props.messages[props.messages.length].createdAt));

    return( 
        <div >
            <span className="header-title"> Oleg Chat </span> 
            <span className="header-users-count"> {userCount}users </span>
            <span className="header-messages-count"> {messageCount}messages</span>
            <span className="header-last-message-date"> { lastFormDate}</span>
            <span className="header-last-message-date" > 
            <NavLink to="/chat">Chat</NavLink>
            </span>
            <span className="header-last-message-date" > 
            <NavLink to="/users">Users</NavLink>
            </span>
            <span className="header-last-message-date" > 
            <NavLink to="/login">login</NavLink>
            </span>
            
        </div>

    )
}
export default Header