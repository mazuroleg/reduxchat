import { useDispatch } from "react-redux";
import {removeMessage} from "../../../rootReducer/MessageSlice"
import "./OwnMessages.css"

const OwnMessage=({id, userId, avatar, user, text, createdAt,editedAt})=>{
    const dispatch=useDispatch();
    let date=new Date(createdAt);
    let formdate=date.getHours()+":"+date.getMinutes();
    
    return( 
        <div className="own-message">
            <div className="message-text">{user}   {text}</div>
            
            <div className="message-time">{formdate}</div>
            <button className="message-edit" >  edit</button>
            <button className="message-delete" onClick={()=>dispatch(removeMessage({userId}))}>  delete</button>
            
            
        </div>

    )
}
export default OwnMessage