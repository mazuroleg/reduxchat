import "./messageInput.css"
import { useDispatch } from "react-redux"
import { addMessage } from "../../../rootReducer/MessageSlice"
import { useState } from "react"
const MessageInput=()=>{
const dispatch=useDispatch();
const [text,setNewMessageText]=useState("")
const addMessageText=()=>{
    dispatch(addMessage({text}))
    setNewMessageText("")
  };

    return( 
        <div className="message-input">
           <div >
               <input type="text"
               className="messages-input-text"
               placeholder="message"
               value={text}
               onChange={event=>setNewMessageText(event.target.value)}
              //onKeyPress={addMessageText}
               />
               

           </div> 
           <div>
            
                <button className="message-input-button" onClick={addMessageText}  >  Send </button>
            </div> 
        </div>

    )
}
export default MessageInput